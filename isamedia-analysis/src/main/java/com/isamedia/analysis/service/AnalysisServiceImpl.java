package com.isamedia.analysis.service;

import com.isamedia.common.command.Command;
import com.isamedia.common.service.analysis.AnalysisService;
import com.isamedia.common.service.repository.analysis.AnalysisRepositoryProxyService;
import com.isamedia.common.type.FileAnalysis;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import lombok.extern.log4j.Log4j2;

import java.util.Arrays;
import java.util.List;

@Log4j2
public class AnalysisServiceImpl implements AnalysisService {

    private static final String FFPROBE_EXECUTABLE_PATH = "ffmpeg/win64/ffprobe.exe";

    private static final List<String> FFPROBE_ARGS = Arrays.asList("-v quiet -show_streams -show_format -sexagesimal -print_format json".split(" "));

    private AnalysisRepositoryProxyService analysisRepositoryService = new AnalysisRepositoryProxyService();

    @Override
    public void analysis(String filePath) {
        analysisRepositoryService.find(filePath, (Handler<AsyncResult<FileAnalysis>>) handler -> {
            if (handler.succeeded()) {
                log.debug("file analysis already exists " + filePath);
            } else {
                new Command(FFPROBE_EXECUTABLE_PATH).args(FFPROBE_ARGS).arg(filePath).run(output -> {
                    analysisRepositoryService.saveRaw(filePath, output);
                });
            }
        });
    }

}
