package com.isamedia.analysis.verticle;

import com.isamedia.analysis.service.AnalysisServiceImpl;
import com.isamedia.common.service.analysis.AnalysisService;
import com.isamedia.common.vertx.ServiceRegistry;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class AnalysisVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        ServiceRegistry.register(vertx, AnalysisService.SERVICE_ADDRESS, new AnalysisServiceImpl());
        startFuture.complete();
    }

}
