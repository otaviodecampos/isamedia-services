package com.isamedia.repository.db;

import org.junit.Assert;
import org.junit.Test;

public class RepositoryTest {

    @Test
    public void stringValue() {
        RepositoryTransaction<String> repository = new MemoryRepository<>("test");
        String key = "key";
        String value = "value";
        repository.getOrDefault(key, value);
        repository.commit();
        Assert.assertEquals(value, repository.get(key));
    }

    @Test
    public void changeValueBeforeCommit() {
        RepositoryTransaction<TestObject> repository = new MemoryRepository<>("test1");
        String key = "key";
        TestObject testObject = repository.getOrDefault(key, new TestObject(key));
        testObject.getValues().add("value1");
        repository.commit();
        Assert.assertEquals(1, repository.get(key).getValues().size());
    }

}
