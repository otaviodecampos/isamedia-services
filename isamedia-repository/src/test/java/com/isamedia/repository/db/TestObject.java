package com.isamedia.repository.db;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@RequiredArgsConstructor
public class TestObject implements Serializable {

    @NonNull
    private String field;

    private List<String> values = new ArrayList<>();

}
