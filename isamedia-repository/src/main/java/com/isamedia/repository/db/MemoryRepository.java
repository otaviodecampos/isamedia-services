package com.isamedia.repository.db;

import org.mapdb.DB;
import org.mapdb.DBMaker;

public class MemoryRepository<T> extends RepositoryTransaction<T> {

    public MemoryRepository(String key) {
        super(key);
    }

    @Override
    protected DB makeDB() {
        return DBMaker.memoryDB().transactionEnable().make();
    }

}
