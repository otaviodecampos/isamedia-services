package com.isamedia.repository.db;

import org.mapdb.DB;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class RepositoryTransaction<T> {

    private static final String DEFAULT_KEY = "default";

    private static DB db;

    private Map<String, T> repository;

    private Map<String, T> values = new HashMap<>();

    @SuppressWarnings("unchecked")
    public RepositoryTransaction(String key) {
        synchronized (RepositoryTransaction.class) {
            if (db == null || db.isClosed()) {
                db = makeDB();
            }
        }
        repository = (Map<String, T>) db.hashMap(key).createOrOpen();
    }

    protected abstract DB makeDB();

    public Set<Map.Entry<String, T>> getAll() {
        return repository.entrySet();
    }

    public Set<String> getKeys() {
        return repository.keySet();
    }

    public T getDefaultKey() {
        return get(DEFAULT_KEY);
    }

    public T get(String key) {
        return getOrDefault(key, null);
    }

    public T getDefaultKeyOrDefault(T defaultValue) {
        return getOrDefault(DEFAULT_KEY, defaultValue);
    }

    public T getOrDefault(String key, T defaultValue) {
        T value = values.get(key);
        if (value == null) {
            value = repository.getOrDefault(key, defaultValue);
            values.put(key, value);
        }
        return value;
    }

    public void set(T value) {
        values.put(DEFAULT_KEY, value);
    }

    public void set(String key, T value) {
        values.put(key, value);
    }

    public void remove(String key) {
        repository.remove(key);
    }

    public void commit() {
        for (String key : values.keySet()) {
            repository.put(key, values.get(key));
        }
        values.clear();
        db.commit();
    }

    public void close() {
        if (!isCommited()) {
            commit();
        }
        db.close();
    }

    private boolean isCommited() {
        return values.isEmpty();
    }

}
