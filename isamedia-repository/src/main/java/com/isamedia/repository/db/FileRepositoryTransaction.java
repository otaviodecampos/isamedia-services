package com.isamedia.repository.db;

import org.mapdb.DB;
import org.mapdb.DBMaker;

public class FileRepositoryTransaction<T> extends RepositoryTransaction<T> {

    private static final String REPOSITORY_FILE_NAME = "isamedia.db";

    public FileRepositoryTransaction(String key) {
        super(key);
    }

    @Override
    protected DB makeDB() {
        return DBMaker.fileDB(REPOSITORY_FILE_NAME).fileMmapEnableIfSupported().transactionEnable().make();
    }

}
