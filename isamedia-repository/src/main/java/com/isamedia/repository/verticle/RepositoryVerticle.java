package com.isamedia.repository.verticle;

import com.isamedia.common.service.repository.analysis.AnalysisRepositoryService;
import com.isamedia.common.service.repository.fileinfo.FileInfoRepositoryService;
import com.isamedia.common.service.repository.settings.SettingsRepositoryService;
import com.isamedia.common.vertx.ServiceRegistry;
import com.isamedia.repository.service.AnalysisRepositoryServiceImpl;
import com.isamedia.repository.service.FileInfoRepositoryServiceImpl;
import com.isamedia.repository.service.SettingsRepositoryServiceImpl;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class RepositoryVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        ServiceRegistry.register(vertx, AnalysisRepositoryService.SERVICE_ADDRESS, new AnalysisRepositoryServiceImpl());
        ServiceRegistry.register(vertx, SettingsRepositoryService.SERVICE_ADDRESS, new SettingsRepositoryServiceImpl());
        ServiceRegistry.register(vertx, FileInfoRepositoryService.SERVICE_ADDRESS, new FileInfoRepositoryServiceImpl());
        startFuture.complete();
    }

}
