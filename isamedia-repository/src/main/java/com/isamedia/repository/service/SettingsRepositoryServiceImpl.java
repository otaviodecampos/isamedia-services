package com.isamedia.repository.service;

import com.isamedia.common.service.repository.settings.SettingsRepositoryService;
import com.isamedia.common.type.Settings;
import com.isamedia.common.type.Source;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class SettingsRepositoryServiceImpl implements SettingsRepositoryService {

    @Override
    public void find(String path, Handler<AsyncResult<Settings>> handler) {
        Settings settings = new Settings();
        settings.getSources().add(new Source("D:\\Testes"));
        handler.handle(Future.succeededFuture(settings));
    }

    @Override
    public void save(String path, Settings data, Handler<AsyncResult<Settings>> handler) {
        log.info("not implemented");
        handler.handle(Future.succeededFuture(null));
    }

    @Override
    public void remove(String path, Handler<AsyncResult<Settings>> handler) {
        log.info("not implemented");
        handler.handle(Future.succeededFuture(null));
    }

}
