package com.isamedia.repository.service;

import com.isamedia.common.future.Futures;
import com.isamedia.common.service.repository.fileinfo.FileInfoRepositoryService;
import com.isamedia.common.type.FileInfo;
import com.isamedia.repository.db.FileRepositoryTransaction;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class FileInfoRepositoryServiceImpl implements FileInfoRepositoryService {

    @Override
    public void find(FileInfo fileInfo, Handler<AsyncResult<FileInfo>> handler) {
        FileRepositoryTransaction<FileInfo> repository = new FileRepositoryTransaction<>(fileInfo.getType().name().toLowerCase());
        fileInfo = repository.get(fileInfo.getPath());
        handler.handle(fileInfo == null ? Future.failedFuture("not found") : Future.succeededFuture(fileInfo));
    }

    @Override
    public void save(FileInfo fileInfo, FileInfo data, Handler<AsyncResult<FileInfo>> handler) {
        save(fileInfo, handler);
    }

    public void save(FileInfo fileInfo, Handler<AsyncResult<FileInfo>> handler) {
        FileRepositoryTransaction<FileInfo> repository = new FileRepositoryTransaction<>(fileInfo.getType().name().toLowerCase());

        if (repository.get(fileInfo.getPath()) != null) {
            handler.handle(Future.failedFuture("already exists"));
            log.trace("fileinfo already exists: {}", fileInfo.getPath());
        } else {
            repository.set(fileInfo.getPath(), fileInfo);
            repository.commit();
            handler.handle(Future.succeededFuture(fileInfo));
            log.debug("fileinfo saved: {}", fileInfo.getPath());
        }
    }

    @Override
    public void remove(FileInfo fileInfo, Handler<AsyncResult<FileInfo>> handler) {
        FileRepositoryTransaction<FileInfo> repository = new FileRepositoryTransaction<>(fileInfo.getType().name().toLowerCase());
        Futures.forEach(repository.getKeys(), (keyEntry, iterationFuture) -> {
            if (keyEntry.startsWith(fileInfo.getPath())) {
                repository.remove(keyEntry);
                log.debug("file info removed: {}", keyEntry);
            }
            iterationFuture.complete();
        }, completionFuture -> {
            repository.commit();
            handler.handle(Future.succeededFuture(null));
        });
    }

}
