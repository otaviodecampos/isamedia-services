package com.isamedia.repository.service;

import com.isamedia.common.file.FileVerify;
import com.isamedia.common.future.Futures;
import com.isamedia.common.service.repository.analysis.AnalysisRepositoryService;
import com.isamedia.common.type.FileAnalysis;
import com.isamedia.common.util.Events;
import com.isamedia.common.vertx.RegisterListener;
import com.isamedia.repository.db.FileRepositoryTransaction;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import lombok.extern.log4j.Log4j2;

import static com.isamedia.common.vertx.CommonEvents.STARTUP_COMPLETE;

@Log4j2
public class AnalysisRepositoryServiceImpl implements AnalysisRepositoryService, RegisterListener {

    private static final String REPOSITORY_KEY = "analysis";

    @Override
    public void onRegister() {
        Events.on(STARTUP_COMPLETE, handler -> refresh());
    }

    @Override
    public void find(String path, Handler<AsyncResult<FileAnalysis>> handler) {
        FileRepositoryTransaction<FileAnalysis> repository = new FileRepositoryTransaction<>(REPOSITORY_KEY);
        FileAnalysis fileAnalysis = repository.get(path);
        handler.handle(fileAnalysis == null ? Future.failedFuture("file analysis not found") : Future.succeededFuture(fileAnalysis));
    }

    @Override
    public void saveRaw(String filePath, String rawAnalysis, Handler<AsyncResult<FileAnalysis>> handler) {
        save(filePath, FileAnalysis.fromJson(rawAnalysis), handler);
    }

    @Override
    public void save(String filePath, FileAnalysis fileAnalysis, Handler<AsyncResult<FileAnalysis>> handler) {
        FileRepositoryTransaction<FileAnalysis> repository = new FileRepositoryTransaction<>(REPOSITORY_KEY);

        if (repository.get(filePath) != null) {
            handler.handle(Future.failedFuture("file analysis already exists"));
            log.trace("file analysis already exists: {}", filePath);
        } else {
            repository.set(filePath, fileAnalysis);
            repository.commit();
            handler.handle(Future.succeededFuture(fileAnalysis));
            log.debug("file analysis saved: {}", filePath);
        }
    }

    @Override
    public void remove(String key, Handler<AsyncResult<FileAnalysis>> handler) {
        FileRepositoryTransaction<FileAnalysis> repository = new FileRepositoryTransaction<>(REPOSITORY_KEY);
        Futures.forEach(repository.getKeys(), (keyEntry, iterationFuture) -> {
            if (keyEntry.startsWith(key)) {
                repository.remove(keyEntry);
                log.debug("file analysis removed: {}", keyEntry);
            }
            iterationFuture.complete();
        }, completionFuture -> {
            repository.commit();
            handler.handle(Future.succeededFuture(null));
        });
    }

    @Override
    public void refresh() {
        FileRepositoryTransaction<FileAnalysis> repository = new FileRepositoryTransaction<>(REPOSITORY_KEY);
        Futures.forEach(repository.getKeys(), (keyEntry, iterationFuture) -> {
            FileVerify.exists(keyEntry, exists -> {
                if (!exists) {
                    repository.remove(keyEntry);
                    log.debug("file analysis removed: {}", keyEntry);
                }
                iterationFuture.complete();
            });
        }, completionFuture -> {
            log.debug("analysis repository refreshed");
            repository.commit();
        });
    }

}
