package com.isamedia.scan.service;

import com.isamedia.common.file.FileProbe;
import com.isamedia.common.file.FileTree;
import com.isamedia.common.file.FileType;
import com.isamedia.common.future.Futures;
import com.isamedia.common.service.analysis.AnalysisProxyService;
import com.isamedia.common.service.monitor.MonitorProxyService;
import com.isamedia.common.service.repository.fileinfo.FileInfoRepositoryProxyService;
import com.isamedia.common.service.repository.settings.SettingsRepositoryProxyService;
import com.isamedia.common.service.scan.ScanService;
import com.isamedia.common.type.Settings;
import com.isamedia.common.type.Source;
import com.isamedia.common.util.Events;
import com.isamedia.common.vertx.RegisterListener;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import lombok.extern.log4j.Log4j2;

import java.util.function.Consumer;

import static com.isamedia.common.util.Strings.EMPTY;
import static com.isamedia.common.vertx.CommonEvents.STARTUP_COMPLETE;

@Log4j2
public class ScanServiceImpl implements ScanService, RegisterListener {

    private SettingsRepositoryProxyService settingsService = new SettingsRepositoryProxyService();

    private FileInfoRepositoryProxyService fileInfoService = new FileInfoRepositoryProxyService();

    private AnalysisProxyService analysisService = new AnalysisProxyService();

    private MonitorProxyService monitorService = new MonitorProxyService();

    @Override
    public void onRegister() {
        Events.on(STARTUP_COMPLETE, handler -> scanSources());
    }

    private void scanSources() {
        settingsService.find(EMPTY, (Consumer<Settings>) settings -> {
            Futures.<Source, Void>forEach(settings.getSources(), (source, future) -> {
                //scan(source.getPath(), future);
            }, scanCompletion -> monitorService.start());
        });
    }

    @Override
    public void scan(String folderPath, Handler<AsyncResult<Void>> handler) {
        FileTree.walk(folderPath, (filePath, walkFuture) -> {
            FileProbe.probeType(filePath, fileType -> {
                if (!fileType.isIgnored()) {
                    if (FileType.VIDEO.equals(fileType)) {
                        analysisService.analysis(filePath);
                        monitorService.monitorFileFolder(filePath);
                    } else {
                        fileInfoService.save(filePath, fileType);
                    }
                }
                walkFuture.complete();
            });
        }).setHandler(result -> {
            if(result.succeeded()) {
                monitorService.monitor(folderPath);
                handler.handle(Future.succeededFuture());
                log.info("Folder scan '{}' done", folderPath);
            } else {
                handler.handle(Future.failedFuture(result.cause()));
            }
        });
    }

}
