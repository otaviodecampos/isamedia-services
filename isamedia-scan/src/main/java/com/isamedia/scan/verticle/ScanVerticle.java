package com.isamedia.scan.verticle;

import com.isamedia.common.service.scan.ScanService;
import com.isamedia.common.vertx.ServiceRegistry;
import com.isamedia.scan.service.ScanServiceImpl;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class ScanVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        ServiceRegistry.register(vertx, ScanService.SERVICE_ADDRESS, new ScanServiceImpl());
        startFuture.complete();
    }

}
