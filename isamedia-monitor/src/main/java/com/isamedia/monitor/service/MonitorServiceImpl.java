package com.isamedia.monitor.service;

import com.isamedia.common.service.monitor.MonitorService;
import com.isamedia.common.vertx.VertxCommon;
import com.isamedia.monitor.event.MonitorKind;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.nio.file.*;
import java.util.HashSet;
import java.util.Set;

import static java.nio.file.StandardWatchEventKinds.*;

@Log4j2
public class MonitorServiceImpl implements MonitorService {

    private static final int MONITOR_INTERVAL = 3000;

    private Set<String> monitors = new HashSet<>();

    private WatchService watchService;

    private long updateTimerId;

    @Override
    public void start() {
        createWatchService();
        execute();
        log.info("Folder monitoring started");
    }

    @Override
    public void monitor(String folderPath) {
        monitors.add(folderPath);
        updateWatchService();
    }

    @Override
    public void unmonitor(String folderPath) {
        monitors.remove(folderPath);
        updateWatchService();
    }

    @Override
    public void monitorFileFolder(String filePath) {
        monitor(Paths.get(filePath).getParent().toString());
    }

    private void registerWatcher(WatchService watcher, String folderPath) throws IOException {
        Paths.get(folderPath).register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
    }

    private void updateWatchService() {
        VertxCommon.vertx.cancelTimer(updateTimerId);
        updateTimerId = VertxCommon.vertx.setTimer(2000, handler -> {
            createWatchService();
            log.info("monitor was updated");
        });
    }

    private void createWatchService() {
        try {
            if (watchService != null) {
                watchService.close();
            }
            watchService = FileSystems.getDefault().newWatchService();
            for (String monitor : monitors) {
                registerWatcher(watchService, monitor);
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

    private void execute() {
        VertxCommon.vertx.cancelTimer(updateTimerId);
        VertxCommon.vertx.setPeriodic(MONITOR_INTERVAL, handler -> {
            WatchKey key = watchService.poll();
            if (key != null) {
                for (WatchEvent<?> event : key.pollEvents()) {
                    Path dir = (Path) key.watchable();
                    MonitorKind.by(event.kind()).getAction().on(dir.resolve(event.context().toString()));
                }
                key.reset();
            }
        });
    }

}
