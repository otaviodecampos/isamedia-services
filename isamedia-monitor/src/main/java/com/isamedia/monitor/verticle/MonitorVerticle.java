package com.isamedia.monitor.verticle;

import com.isamedia.common.service.monitor.MonitorService;
import com.isamedia.common.vertx.ServiceRegistry;
import com.isamedia.monitor.service.MonitorServiceImpl;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;

public class MonitorVerticle extends AbstractVerticle {

    @Override
    public void start(Future startFuture) {
        ServiceRegistry.register(vertx, MonitorService.SERVICE_ADDRESS, new MonitorServiceImpl());
        startFuture.complete();
    }

}
