package com.isamedia.monitor.event;

import java.nio.file.Path;

@FunctionalInterface
public interface MonitorAction {

    void on(Path path);

}
