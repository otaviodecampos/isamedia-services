package com.isamedia.monitor.event;

import com.isamedia.common.file.FileType;
import com.isamedia.common.service.monitor.MonitorProxyService;
import com.isamedia.common.service.repository.analysis.AnalysisRepositoryProxyService;
import com.isamedia.common.service.repository.fileinfo.FileInfoRepositoryProxyService;
import com.isamedia.common.type.FileInfo;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;

@Log4j2
public class MonitorDeletedAction implements MonitorAction {

    private MonitorProxyService monitorService = new MonitorProxyService();

    private AnalysisRepositoryProxyService analysisRepositoryService = new AnalysisRepositoryProxyService();

    private FileInfoRepositoryProxyService fileInfoRepositoryService = new FileInfoRepositoryProxyService();

    @Override
    public void on(Path path) {
        log.info("'{}' was deleted", path);
        analysisRepositoryService.remove(path.toString());
        monitorService.unmonitor(path.toString());
        fileInfoRepositoryService.remove(new FileInfo(path.toString(), FileType.CAPTION));
        fileInfoRepositoryService.remove(new FileInfo(path.toString(), FileType.IMAGE));
    }

}
