package com.isamedia.monitor.event;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.nio.file.WatchEvent;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;

@AllArgsConstructor
public enum MonitorKind {

    NONE(null, new MonitorModifyAction()),
    CREATED(ENTRY_CREATE, new MonitorCreatedAction()),
    DELETED(ENTRY_DELETE, new MonitorDeletedAction());

    private WatchEvent.Kind kind;

    @Getter
    private MonitorAction action;

    public static MonitorKind by(WatchEvent.Kind kind) {
        for (MonitorKind monitorEvent : MonitorKind.values()) {
            if (kind.equals(monitorEvent.kind)) {
                return monitorEvent;
            }
        }
        return NONE;
    }

}
