package com.isamedia.monitor.event;

import com.isamedia.common.file.FileProbe;
import com.isamedia.common.file.FileType;
import com.isamedia.common.service.scan.ScanProxyService;
import lombok.extern.log4j.Log4j2;

import java.nio.file.Path;

@Log4j2
public class MonitorModifyAction implements MonitorAction {

    private ScanProxyService scanService = new ScanProxyService();

    @Override
    public void on(Path path) {
        log.info("'{}' was modified", path);
        FileProbe.probeType(path, fileType -> {
            if (FileType.FOLDER.equals(fileType)) {
                scanService.scan(path.toString());
            }
        });
    }

}
