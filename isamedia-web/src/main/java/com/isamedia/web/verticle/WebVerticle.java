package com.isamedia.web.verticle;

import com.isamedia.common.service.scan.ScanService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class WebVerticle extends AbstractVerticle {

    private int port = 8080;

    @Override
    public void start(Future<Void> future) {
        HttpServer server = vertx.createHttpServer();

        Router router = Router.router(vertx);
        router.route("/*").handler(StaticHandler.create());

        BridgeOptions opts = new BridgeOptions()
            .addInboundPermitted(new PermittedOptions()
                .setAddress(ScanService.SERVICE_ADDRESS))
            .addOutboundPermitted(new PermittedOptions()
                .setAddress(ScanService.SERVICE_ADDRESS))
            .addOutboundPermitted(new PermittedOptions()
                .setAddress("web-service-address"));

        // Create the event bus bridge and add it to the router.
        SockJSHandler ebHandler = SockJSHandler.create(vertx).bridge(opts);
        router.route("/eventbus/*").handler(ebHandler);

        server.requestHandler(router::accept).listen(port, handler -> {
            if (handler.succeeded()) {
                log.info("Succeeded in deploying web verticle on port '{}'", port);
                future.complete();
            } else {
                future.fail(handler.cause());
            }
        });
    }

}
