package com.isamedia.common.service.repository.analysis;

import com.isamedia.common.service.repository.RepositoryService;
import com.isamedia.common.type.FileAnalysis;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

@ProxyGen
public interface AnalysisRepositoryService extends RepositoryService<String, FileAnalysis> {

    String SERVICE_ADDRESS = "com.isamedia.repository:analysis-service";

    void refresh();

    void find(String filePath, Handler<AsyncResult<FileAnalysis>> handler);

    void save(String filePath, FileAnalysis fileAnalysis, Handler<AsyncResult<FileAnalysis>> handler);

    void saveRaw(String filePath, String rawAnalysis, Handler<AsyncResult<FileAnalysis>> handler);

    void remove(String filePath, Handler<AsyncResult<FileAnalysis>> handler);

}
