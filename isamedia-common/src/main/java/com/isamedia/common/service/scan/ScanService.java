package com.isamedia.common.service.scan;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

@VertxGen
@ProxyGen
public interface ScanService {

    String SERVICE_ADDRESS = "com.isamedia.scan:scan-service";

    void scan(String path, Handler<AsyncResult<Void>> handler);

}
