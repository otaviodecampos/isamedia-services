package com.isamedia.common.service.repository.fileinfo;

import com.isamedia.common.file.FileType;
import com.isamedia.common.function.VoidConsumer;
import com.isamedia.common.service.repository.RepositoryProxyService;
import com.isamedia.common.type.FileInfo;
import com.isamedia.common.vertx.VertxCommon;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

public class FileInfoRepositoryProxyService extends RepositoryProxyService<FileInfoRepositoryService, FileInfo, FileInfo> {

    public FileInfoRepositoryService proxy() {
        return new FileInfoRepositoryServiceVertxEBProxy(VertxCommon.vertx, FileInfoRepositoryService.SERVICE_ADDRESS);
    }

    public void save(String path, FileType type) {
        save(new FileInfo(path, type), VoidConsumer::accept);
    }

    public void save(FileInfo fileInfo) {
        save(fileInfo, VoidConsumer::accept);
    }

    public void save(FileInfo fileInfo, Handler<AsyncResult<FileInfo>> handler) {
        proxy().save(fileInfo, fileInfo, handler);
    }

}
