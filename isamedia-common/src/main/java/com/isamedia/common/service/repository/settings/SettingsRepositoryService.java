package com.isamedia.common.service.repository.settings;

import com.isamedia.common.service.repository.RepositoryService;
import com.isamedia.common.type.Settings;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

@ProxyGen
public interface SettingsRepositoryService extends RepositoryService<String, Settings> {

    String SERVICE_ADDRESS = "com.isamedia.repository:settings-service";

    void find(String path, Handler<AsyncResult<Settings>> handler);

    void save(String path, Settings data, Handler<AsyncResult<Settings>> handler);

    void remove(String path, Handler<AsyncResult<Settings>> handler);

}
