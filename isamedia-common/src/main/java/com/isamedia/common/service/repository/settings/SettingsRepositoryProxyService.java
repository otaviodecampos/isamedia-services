package com.isamedia.common.service.repository.settings;

import com.isamedia.common.service.repository.RepositoryProxyService;
import com.isamedia.common.type.Settings;
import com.isamedia.common.vertx.VertxCommon;

public class SettingsRepositoryProxyService extends RepositoryProxyService<SettingsRepositoryService, String, Settings> {

    public SettingsRepositoryService proxy() {
        return new SettingsRepositoryServiceVertxEBProxy(VertxCommon.vertx, SettingsRepositoryService.SERVICE_ADDRESS);
    }

}
