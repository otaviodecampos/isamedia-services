package com.isamedia.common.service.analysis;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;

@VertxGen
@ProxyGen
public interface AnalysisService {

    String SERVICE_ADDRESS = "analysis-service-address";

    void analysis(String path);

}
