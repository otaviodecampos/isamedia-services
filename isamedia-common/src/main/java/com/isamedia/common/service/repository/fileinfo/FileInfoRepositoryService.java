package com.isamedia.common.service.repository.fileinfo;

import com.isamedia.common.service.repository.RepositoryService;
import com.isamedia.common.type.FileInfo;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

@ProxyGen
public interface FileInfoRepositoryService extends RepositoryService<FileInfo, FileInfo> {

    String SERVICE_ADDRESS = "com.isamedia.repository:fileinfo-service";

    void find(FileInfo fileInfo, Handler<AsyncResult<FileInfo>> handler);

    void save(FileInfo fileInfo, FileInfo data, Handler<AsyncResult<FileInfo>> handler);

    void remove(FileInfo fileInfo, Handler<AsyncResult<FileInfo>> handler);

}
