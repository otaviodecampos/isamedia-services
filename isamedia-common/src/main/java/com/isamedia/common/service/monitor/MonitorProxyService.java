package com.isamedia.common.service.monitor;

import com.isamedia.common.vertx.VertxCommon;

public class MonitorProxyService implements MonitorService {

    private static MonitorService proxy() {
        return new MonitorServiceVertxEBProxy(VertxCommon.vertx, MonitorService.SERVICE_ADDRESS);
    }

    @Override
    public void start() {
        proxy().start();
    }

    @Override
    public void monitor(String path) {
        proxy().monitor(path);
    }

    @Override
    public void unmonitor(String path) {
        proxy().unmonitor(path);
    }

    @Override
    public void monitorFileFolder(String filePath) {
        proxy().monitorFileFolder(filePath);
    }

}
