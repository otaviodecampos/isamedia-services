package com.isamedia.common.service.repository.analysis;

import com.isamedia.common.function.VoidConsumer;
import com.isamedia.common.service.repository.RepositoryProxyService;
import com.isamedia.common.type.FileAnalysis;
import com.isamedia.common.vertx.VertxCommon;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class AnalysisRepositoryProxyService extends RepositoryProxyService<AnalysisRepositoryService, String, FileAnalysis> {

    public AnalysisRepositoryService proxy() {
        return new AnalysisRepositoryServiceVertxEBProxy(VertxCommon.vertx, AnalysisRepositoryService.SERVICE_ADDRESS);
    }

    public void saveRaw(String key, String rawAnalysis) {
        proxy().saveRaw(key, rawAnalysis, VoidConsumer::accept);
    }

    public void saveRaw(String key, String rawAnalysis, Handler<AsyncResult<FileAnalysis>> handler) {
        proxy().saveRaw(key, rawAnalysis, handler);
    }

}
