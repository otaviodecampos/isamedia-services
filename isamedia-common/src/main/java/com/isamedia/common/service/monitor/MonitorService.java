package com.isamedia.common.service.monitor;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.codegen.annotations.VertxGen;

@VertxGen
@ProxyGen
public interface MonitorService {

    String SERVICE_ADDRESS = "com.isamedia.scan:monitor-service";

    void start();

    void monitor(String path);

    void unmonitor(String path);

    void monitorFileFolder(String filePath);
}
