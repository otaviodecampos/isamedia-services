package com.isamedia.common.service.analysis;

import com.isamedia.common.vertx.VertxCommon;

public class AnalysisProxyService implements AnalysisService {

    private static AnalysisService proxy() {
        return new AnalysisServiceVertxEBProxy(VertxCommon.vertx, AnalysisService.SERVICE_ADDRESS);
    }

    @Override
    public void analysis(String path) {
        proxy().analysis(path);
    }
}
