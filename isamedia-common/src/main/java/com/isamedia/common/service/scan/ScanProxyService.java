package com.isamedia.common.service.scan;

import com.isamedia.common.function.VoidConsumer;
import com.isamedia.common.vertx.VertxCommon;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

public class ScanProxyService implements ScanService {

    private static ScanService proxy() {
        return new ScanServiceVertxEBProxy(VertxCommon.vertx, ScanService.SERVICE_ADDRESS);
    }

    public void scan(String path) {
        scan(path, VoidConsumer::accept);
    }

    @Override
    public void scan(String path, Handler<AsyncResult<Void>> handler) {
        proxy().scan(path, handler);
    }

}
