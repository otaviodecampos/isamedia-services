package com.isamedia.common.service.repository;

import com.isamedia.common.function.VoidConsumer;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

import java.util.function.Consumer;

public abstract class RepositoryProxyService<S extends RepositoryService<K, T>, K, T> {

    public abstract S proxy();

    public void find(K key, Consumer<T> consumer) {
        proxy().find(key, handler -> consumer.accept(handler.result()));
    }

    public void find(K key, Handler<AsyncResult<T>> handler) {
        proxy().find(key, handler);
    }

    public void save(K key, T data, Handler<AsyncResult<T>> handler) {
        proxy().save(key, data, handler);
    }

    public void remove(K key) {
        remove(key, VoidConsumer::accept);
    }

    public void remove(K key, Handler<AsyncResult<T>> handler) {
        proxy().remove(key, handler);
    }

}
