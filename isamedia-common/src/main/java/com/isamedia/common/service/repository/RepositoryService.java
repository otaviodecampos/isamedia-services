package com.isamedia.common.service.repository;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

public interface RepositoryService<K, T> {

    void find(K path, Handler<AsyncResult<T>> handler);

    void save(K path, T data, Handler<AsyncResult<T>> handler);

    void remove(K path, Handler<AsyncResult<T>> handler);

}
