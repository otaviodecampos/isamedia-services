package com.isamedia.common.util;

import com.isamedia.common.vertx.VertxCommon;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;

public class Events {

    public static void send(String address) {
        send(address, null);
    }

    public static void send(String address, Object content) {
        VertxCommon.vertx.eventBus().publish(address, content);
    }

    public static <T> void on(String address, Handler<Message<T>> handler) {
        VertxCommon.vertx.eventBus().consumer(address, teste -> {
            handler.handle(null);
        });
    }

}
