package com.isamedia.common.util;

import lombok.extern.log4j.Log4j2;

import java.lang.reflect.Field;

@Log4j2
public class Objects {

    public static void copyValues(Object base, Object target) {
        for (Field field : target.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                field.set(target, field.get(base));
            } catch (IllegalAccessException e) {
                log.error(e);
            } finally {
                field.setAccessible(false);
            }
        }
    }

}
