package com.isamedia.common.util;

import com.isamedia.common.exception.ClassInstantiationException;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class Classes {

    public static <T> T instantiate(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            log.error(e.getMessage(), e);
        }
        throw new ClassInstantiationException();
    }

}
