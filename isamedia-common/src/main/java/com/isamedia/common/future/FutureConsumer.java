package com.isamedia.common.future;

import io.vertx.core.Future;

@FunctionalInterface
public interface FutureConsumer<T, F> {

    void accept(T var1, Future<F> future);

}
