package com.isamedia.common.future;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

public class Futures<T> {

    private List<Future> futures = new ArrayList<>();

    public Future<T> future() {
        Future<T> future = Future.future();
        futures.add(future);
        return future;
    }

    public CompositeFuture all(Future future) {
        return CompositeFuture.all(futures).setHandler(result -> {
            if (result.succeeded()) {
                future.complete();
            } else {
                future.fail(result.cause());
            }
        });
    }

    public static <T, F> Futures forEach(Collection<T> list, FutureConsumer<T, F> consumer) {
        Futures<F> itemCompletions = new Futures<>();
        list.forEach(item -> consumer.accept(item, itemCompletions.future()));
        return itemCompletions;
    }

    public static <T, F> void forEach(Collection<T> list, FutureConsumer<T, F> consumer, Future completionFuture) {
        forEach(list, consumer).all(completionFuture);
    }

    public static <T, F> void forEach(Collection<T> list, FutureConsumer<T, F> consumer, Consumer<Void> completionConsumer) {
        Future<Void> future = Future.future();
        forEach(list, consumer).all(future.setHandler(handler -> {
            completionConsumer.accept(null);
        }));
    }

}
