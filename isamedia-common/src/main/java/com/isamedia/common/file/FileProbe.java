package com.isamedia.common.file;

import com.isamedia.common.vertx.VertxCommon;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Consumer;

@Log4j2
public class FileProbe {

    public static void probeType(String path, Consumer<FileType> consumer) {
        probeType(new File(path), consumer);
    }

    public static void probeType(File file, Consumer<FileType> consumer) {
        probeType(file.toPath(), consumer);
    }

    public static void probeType(Path path, Consumer<FileType> consumer) {
        VertxCommon.vertx.executeBlocking(handler -> {
            FileVerify.isDirectory(path, isDir -> {
                if (isDir) {
                    handler.complete(FileType.FOLDER);
                } else {
                    try {
                        handler.complete(FileType.getFileType(Files.probeContentType(path)));
                    } catch (IOException e) {
                        log.error(e.getMessage(), e);
                        handler.fail(e);
                    }
                }
            });
        }, resultHandler -> {
            consumer.accept((FileType) resultHandler.result());
        });
    }

}
