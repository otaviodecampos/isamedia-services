package com.isamedia.common.file;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;

@Log4j2
@NoArgsConstructor
@AllArgsConstructor
public enum FileType {

    IGNORED,
    VIDEO("video"),
    CAPTION("text"),
    IMAGE("image"),
    FOLDER("folder");

    private String mimeTypePrefix;

    public static FileType getFileType(String mimeType) {
        for (FileType fileType : FileType.values()) {
            if (fileType.mimeTypePrefix != null && mimeType.startsWith(fileType.mimeTypePrefix)) {
                return fileType;
            }
        }
        log.trace("file type {} was ignored", mimeType);
        return IGNORED;
    }

    public boolean isIgnored() {
        return this.equals(IGNORED);
    }

}
