package com.isamedia.common.file;

import com.isamedia.common.vertx.VertxCommon;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.nio.file.Path;
import java.util.function.Consumer;

@Log4j2
public class FileVerify {

    public static void exists(String path, Consumer<Boolean> consumer) {
        exists(new File(path), consumer);
    }

    public static void exists(File file, Consumer<Boolean> consumer) {
        exists(file.toPath(), consumer);
    }

    public static void exists(Path path, Consumer<Boolean> consumer) {
        VertxCommon.vertx.executeBlocking(handler -> {
            handler.complete(path.toFile().exists());
        }, resultHandler -> {
            consumer.accept((Boolean) resultHandler.result());
        });
    }

    public static void isDirectory(Path path, Consumer<Boolean> consumer) {
        VertxCommon.vertx.executeBlocking(handler -> {
            handler.complete(path.toFile().isDirectory());
        }, resultHandler -> {
            consumer.accept((Boolean) resultHandler.result());
        });
    }

}
