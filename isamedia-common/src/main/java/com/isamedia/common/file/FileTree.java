package com.isamedia.common.file;

import com.isamedia.common.future.FutureConsumer;
import com.isamedia.common.future.Futures;
import com.isamedia.common.vertx.VertxCommon;
import io.vertx.core.Future;
import io.vertx.core.file.FileSystem;

import java.util.function.Consumer;


public class FileTree {

    private static FileSystem fileSystem = VertxCommon.vertx.fileSystem();

    public static Future<Void> walk(String folderPath, FutureConsumer<String, Void> resultConsumer) {
        Future<Void> walkFuture = Future.future();
        fileSystem.readDir(folderPath, handler -> {
            if (handler.succeeded()) {
                Futures.<String, Void>forEach(handler.result(), (path, iterationFuture) -> {
                    fileSystem.props(path, handlerProps -> {
                        if (handlerProps.result().isDirectory()) {
                            walk(path, resultConsumer).setHandler(complete -> {
                                iterationFuture.complete();
                            });
                        } else {
                            resultConsumer.accept(path, iterationFuture);
                        }
                    });
                }, walkFuture);
            } else {
                walkFuture.fail(handler.cause());
            }
        });
        return walkFuture;
    }

}
