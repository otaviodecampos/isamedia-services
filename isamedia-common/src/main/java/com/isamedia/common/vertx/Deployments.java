package com.isamedia.common.vertx;

import com.isamedia.common.future.FutureConsumer;
import com.isamedia.common.future.Futures;
import com.isamedia.common.util.Classes;
import io.vertx.core.*;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class Deployments {
    
    private Futures<String> verticleCompletions = new Futures<>();

    public Deployments deploy(Class<? extends Verticle> verticleClass) {
        return deploy(Classes.instantiate(verticleClass));
    }

    public Deployments deploy(Verticle verticle) {
        Future<String> deployFuture = Future.future();
        deployFuture.compose(handler -> {
            log.trace("Verticle '{}' was deployed", verticle.getClass().getSimpleName());
        }, verticleCompletions.future());
        VertxCommon.vertx.deployVerticle(verticle, deployFuture);
        return this;
    }

    public Deployments deploy(Class<? extends Verticle> verticleClass, FutureConsumer<Deployments, String> consumer) {
        return deploy(Classes.instantiate(verticleClass), consumer);
    }

    public Deployments deploy(Verticle verticle, FutureConsumer<Deployments, String> consumer) {
        Future<String> completion = verticleCompletions.future();
        VertxCommon.vertx.deployVerticle(verticle, handler -> consumer.accept(this, completion));
        return this;
    }

    public Deployments deploy(Class<? extends Verticle> verticleClass, int instances) {
        VertxCommon.vertx.deployVerticle(verticleClass, new DeploymentOptions().setInstances(instances));
        return this;
    }

    public Deployments deploy(Class<? extends Verticle> verticleClass, int instances, Handler<AsyncResult<String>> handler) {
        VertxCommon.vertx.deployVerticle(verticleClass, new DeploymentOptions().setInstances(instances), handler);
        return this;
    }

    public Deployments deployWorker(Class<? extends Verticle> verticleClass) {
        VertxCommon.vertx.deployVerticle(verticleClass, new DeploymentOptions().setWorker(true));
        return this;
    }

    public void complete(Handler<Void> completeHandler, Future<Void> future) {
        Future<Void> completeFuture = Future.future();
        completeFuture.setHandler(handler -> {
            log.trace("Deployments are complete");
            completeHandler.handle(null);
            future.complete();
        });
        verticleCompletions.all(completeFuture);
    }

}
