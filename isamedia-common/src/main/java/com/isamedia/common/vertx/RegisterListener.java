package com.isamedia.common.vertx;

public interface RegisterListener {

    void onRegister();

}
