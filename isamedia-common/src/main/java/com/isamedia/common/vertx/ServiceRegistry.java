package com.isamedia.common.vertx;

import io.vertx.core.Vertx;
import io.vertx.serviceproxy.ServiceBinder;

public class ServiceRegistry {

    @SuppressWarnings("unchecked")
    public static <T> void register(Vertx vertx, String serviceAddress, T serviceInstance) {
        new ServiceBinder(vertx).setAddress(serviceAddress).register((Class<T>) serviceInstance.getClass().getInterfaces()[0], serviceInstance);
        if (serviceInstance instanceof RegisterListener) {
            ((RegisterListener) serviceInstance).onRegister();
        }
    }

}
