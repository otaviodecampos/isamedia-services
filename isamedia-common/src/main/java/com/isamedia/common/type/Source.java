package com.isamedia.common.type;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
public class Source extends SerializableType {

    @NonNull
    private String path;

}
