package com.isamedia.common.type;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class FileAnalysisStream extends SerializableType {

    private int index;

    @JsonProperty("codec_type")
    private FileAnalysisStreamType type;

    private int width;

    private int height;

    private Map<String, String> disposition = new HashMap<>();

    private Map<String, String> tags = new HashMap<>();

}
