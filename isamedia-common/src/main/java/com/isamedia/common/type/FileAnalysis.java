package com.isamedia.common.type;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@DataObject
@NoArgsConstructor
public class FileAnalysis extends SerializableType {

    private List<FileAnalysisStream> streams = new ArrayList<>();

    private FileAnalysisFormat format = new FileAnalysisFormat();

    public FileAnalysis(JsonObject jsonObject) {
        fromJson(jsonObject);
    }

    public static FileAnalysis fromJson(String json) {
        return SerializableType.fromJson(json, FileAnalysis.class);
    }

}
