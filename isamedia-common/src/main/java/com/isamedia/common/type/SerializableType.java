package com.isamedia.common.type;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.isamedia.common.util.Objects;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public class SerializableType implements Serializable {

    public JsonObject toJson() {
        return JsonObject.mapFrom(this);
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        Json.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return Json.decodeValue(json, clazz);
    }

    void fromJson(JsonObject jsonObject) {
        Json.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        Objects.copyValues(Json.decodeValue(jsonObject.encode(), this.getClass()), this);
    }

}
