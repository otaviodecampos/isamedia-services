package com.isamedia.common.type;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FileAnalysisFormat extends SerializableType {

    private String duration = "0:00:00.000000";

    private Long size;

}
