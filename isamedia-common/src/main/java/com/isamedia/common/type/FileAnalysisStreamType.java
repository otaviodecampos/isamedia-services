package com.isamedia.common.type;

import com.fasterxml.jackson.annotation.JsonCreator;

import java.io.Serializable;

public enum FileAnalysisStreamType implements Serializable {

    VIDEO, AUDIO, SUBTITLE, ATTACHMENT, UNKNOWN;

    @JsonCreator
    public static FileAnalysisStreamType from(String value) {
        FileAnalysisStreamType type;
        try {
            type = FileAnalysisStreamType.valueOf(value.toUpperCase());
        } catch (IllegalArgumentException e) {
            type = UNKNOWN;
        }
        return type;
    }

}
