package com.isamedia.common.type;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@DataObject
@NoArgsConstructor
public class Settings extends SerializableType {

    private List<Source> sources = new ArrayList<>();

    public Settings(JsonObject jsonObject) {
        fromJson(jsonObject);
    }

}
