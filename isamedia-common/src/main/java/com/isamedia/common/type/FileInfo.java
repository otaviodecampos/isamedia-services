package com.isamedia.common.type;

import com.isamedia.common.file.FileType;
import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;
import lombok.*;

@Getter
@Setter
@DataObject
@NoArgsConstructor
@RequiredArgsConstructor
public class FileInfo extends SerializableType {

    @NonNull
    private String path;

    @NonNull
    private FileType type;

    public FileInfo(JsonObject jsonObject) {
        fromJson(jsonObject);
    }

}
