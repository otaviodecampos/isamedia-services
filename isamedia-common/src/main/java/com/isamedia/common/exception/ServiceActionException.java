package com.isamedia.common.exception;

public class ServiceActionException extends RuntimeException {

    public ServiceActionException(Throwable var1) {
        super(var1);
    }

}
