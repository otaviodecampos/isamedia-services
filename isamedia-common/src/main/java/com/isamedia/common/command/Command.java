package com.isamedia.common.command;

import com.isamedia.common.vertx.VertxCommon;
import com.julienviet.childprocess.Process;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.buffer.impl.BufferImpl;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Getter
@RequiredArgsConstructor
public class Command {

    @NonNull
    private final String executable;

    private final List<String> args = new ArrayList<>();

    public Command arg(String arg) {
        this.args.add(arg);
        return this;
    }

    public Command args(List<String> args) {
        this.args.addAll(args);
        return this;
    }

    public void run(Consumer<String> outputConsumer) {
        Buffer outputBuffer = new BufferImpl();
        Process process = Process.spawn(VertxCommon.vertx, executable, args);
        process.stdout().handler(outputBuffer::appendBuffer);
        process.exitHandler(handler -> {
            outputConsumer.accept(outputBuffer.toString());
        });
        process.kill();
    }

}
