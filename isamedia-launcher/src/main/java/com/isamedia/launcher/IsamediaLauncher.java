package com.isamedia.launcher;

import io.vertx.core.Launcher;

public class IsamediaLauncher {

    private static final String STARTUP_VERTICLE_NAME = "com.isamedia.launcher.verticle.StartupVerticle";

    private static final String VERTX_LOGGER_FACTORY_PROPERTY = "vertx.logger-delegate-factory-class-name";

    private static final String VERTX_LOGGER_FACTORY_CLASS = "io.vertx.core.logging.Log4j2LogDelegateFactory";

    public static void main(String[] args) {
        setVertxLoggerAsLog4j();
        new Launcher().dispatch(new String[]{"run", STARTUP_VERTICLE_NAME});
    }

    private static void setVertxLoggerAsLog4j() {
        System.setProperty(VERTX_LOGGER_FACTORY_PROPERTY, VERTX_LOGGER_FACTORY_CLASS);
    }

}
