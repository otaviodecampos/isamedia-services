package com.isamedia.launcher.verticle;

import com.isamedia.analysis.verticle.AnalysisVerticle;
import com.isamedia.common.util.Events;
import com.isamedia.common.vertx.Deployments;
import com.isamedia.common.vertx.VertxCommon;
import com.isamedia.monitor.verticle.MonitorVerticle;
import com.isamedia.repository.verticle.RepositoryVerticle;
import com.isamedia.scan.verticle.ScanVerticle;
import com.isamedia.web.verticle.WebVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import lombok.extern.log4j.Log4j2;

import static com.isamedia.common.vertx.CommonEvents.STARTUP_COMPLETE;

@Log4j2
public class StartupVerticle extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) {
        VertxCommon.vertx = vertx;

        new Deployments()
            .deploy(WebVerticle.class)
            .deploy(AnalysisVerticle.class)
            .deploy(ScanVerticle.class)
            .deploy(MonitorVerticle.class)
            .deploy(RepositoryVerticle.class)
            .complete(handler -> Events.send(STARTUP_COMPLETE), startFuture);
    }

}
